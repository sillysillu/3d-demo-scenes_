# README #

### What is this repository for? ###
* Version: Demo Demo. :)
* Link to corresponding repository 'release' of [Demo Demo](https://bitbucket.org/sillysillu/3d-demo-scenes/commits/407fa269d4301261a91b0267cd3a152c9c3d4759)
* Link to corresponding repository 'release' of [Final Demo](https://bitbucket.org/sillysillu/3d-demo-scenes_/commits/d8748ee2610f4ca621bde8fae1481e2785f5f7be)
* [More information](https://courses.cs.ut.ee/2016/cg/fall/Main/Project-3DDemoScenes)

### How do I get set up? ###
As we currently do not have a standalone build, you need to:  

* Download Unreal Engine 4.14  
* Download/Clone [repository](https://bitbucket.org/sillysillu/3d-demo-scenes/downloads)  
* Open project from repository with existing project selection in UE. 
* Finally, open a level.
    * There is currently one builded level: Lobby
    * And 2 unbuilded levels: Lonely Island & Forest Path
* Due lacking of enough space in Bitbucket, builddata will be provided via Dropbox link.
* [Lonely Island builddata](https://www.dropbox.com/s/kiq9pt8h3u3ikz3/LonelyIsland_BuiltData.uasset?dl=0)
* [Forest Path builddata](https://www.dropbox.com/s/g4wwoqp4cypx3uf/ForestPath_BuiltData.uasset?dl=0)
* [Final Demo Release - Presentation Standalone](https://www.dropbox.com/s/o8i502ebrj4wbwv/3DDemoScenes.7z?dl=0)

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact